<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
        <script type="text/javascript" async="" src="<?php echo get_theme_file_uri("/images/Slider/slide2.jpg");?>js/page.js"></script>
        <footer class="footer-con">            
            <div class="middleWrapper">
                <div class="mb-logo-footer"> 
                    <a href="<?php echo site_url();?>/privacy-policy/" target="_blank" class="footer-link">Privacy & Policy</a> &#8226; 
                    <a href="<?php echo site_url();?>/terms-of-use/" target="_blank" class="footer-link">Terms of use</a>
                </div>
                <div class="copyRightInfo">ShreeNand.com - All trademarks, logos and names are properties of their respective owners. All Rights Reserved. © Copyright
                    <script type="text/javascript">                        
                        document.write(new Date().getFullYear())
                    </script> Shree Nand 
                </div>
            </div>
        </footer>
        <div class="form">
            <h2>Contact Us <div class="close">x</div></h2>
            <div id="fromContainer">
                <div class="formLeftCon">
                    <div class="field-div"><input type="text" name="name" id="name" placeholder="Name"></div>
                    <div class="field-div"><input type="text" name="email" id="email" placeholder="Email"></div>
                    <div class="field-div"><textarea id="message" rows="4" cols="12" placeholder="Message"></textarea></div>
                    <div class="field-div"><button class="contact sm-con" id="submit">Submit</button></div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="<?php echo get_theme_file_uri("/js/jquery.slimscroll.js");?>"></script>
        <!--<script type="text/javascript" src="js/jquery.slimscroll.js(1).download"></script>-->
        <script type="text/javascript" src="<?php echo get_theme_file_uri("/js/lightbox.js");?>"></script>
        <script type="text/javascript">
            winW = $(window).width();

            function scrollNav() {
                $('.navigation a').click(function() {
                    var theClass = $(this).attr("class");
                    $('.navigation a').parents('li').removeClass('textDecoration');
                    $(this).parents('li').addClass('textDecoration');
                    $('html, body').stop().animate({
                        scrollTop: $($(this).attr('href')).offset().top - 50
                    }, 400);
                    return false;

                });
            }



            function scrollNav1() {
                    $('.navigation a').click(function() {
                        var theClass = $(this).attr("class");

                        $('.navigation').slideUp();
                        $('html, body').stop().animate({
                            scrollTop: $($(this).attr('href')).offset().top - 50
                        }, 400);
                        return false;

                    });
                }
                //scrollNav();

            if (winW < 400 || winW < 1024) {

                scrollNav1();

            } else {
                scrollNav();
            }



            $(window).scroll(function() {
                var topscrop = $(window).scrollTop()
                if (topscrop > 200) {
                    $('.w100').addClass('shadow')
                } else {
                    $('.w100').removeClass('shadow')

                }
            })


            docH = $(document).height();
            $(function() {
                $('#testDiv').slimScroll({
                    alwaysVisible: true,
                    railVisible: true
                })
            });
            $('.bar').click(function() {
                $('.navigation').slideToggle();


            })




            $('.read').click(function() {

                $(this).toggleClass('less')
                if ($(this).hasClass('less')) {
                    $(this).text('Read More');
                } else {
                    $(this).text('Read Less');
                    console.log('sss')
                }
                $('.readMore').slideToggle();

            })

            $('.contact,.topcontact').click(function() {
                $('.form,.overlay').show();
                $('.overlay').css('height', docH);
            })
            $('.close,.overlay').click(function() {
                $('.form,.overlay').hide();
            })




            /*Tab JS Start*/
            x = 1;
            f = 1;
            s = 1;
            t = 1;
            k = 1;
            l = 1;
            p = 1;
            q = 1;
            r = 1;;
            w = 1;
            y = 1;
            u = 1;
            m = 1;
            n = 1;
            $('.mainTab li').each(function() {
                $(this).attr('rel', 'tab' + x);
                x++
            });
            $('.firstTab li').each(function() {
                $(this).attr('rel', 'firstTab' + f);
                f++
            })
            $('.secondTab li').each(function() {
                $(this).attr('rel', 'secondTab' + s);
                s++
            })
            $('.thirdTab li').each(function() {
                $(this).attr('rel', 'thirdTab' + t);
                t++
            })
            $('.fourTab li').each(function() {
                $(this).attr('rel', 'fourTab' + k);
                k++
            })
            $('.fiveTab li').each(function() {
                $(this).attr('rel', 'fiveTab' + l);
                l++
            })
            $('.sixTab li').each(function() {
                $(this).attr('rel', 'sixTab' + p);
                p++
            })
            $('.sevenTab li').each(function() {
                $(this).attr('rel', 'sevenTab' + q);
                q++
            })
            $('.eightTab li').each(function() {
                $(this).attr('rel', 'eightTab' + r);
                r++
            })
            $('.nineTab li').each(function() {
                $(this).attr('rel', 'nineTab' + w);
                w++
            })
            $('.tenTab li').each(function() {
                $(this).attr('rel', 'tenTab' + y);
                y++
            })
            $('.elevenTab li').each(function() {
                $(this).attr('rel', 'elevenTab' + u);
                u++
            })
            $('.mapView li').each(function() {
                $(this).attr('rel', 'mapTab' + m);
                m++
            })
            $('.mainTab li:first-child').addClass('active');
            $('.mainTab li').click(function() {
                $('.mainTab li').removeClass('active');
                $(this).addClass('active');
                $('.nodisplay').hide();
                var currTab = $(this).attr('rel');
                $('#' + currTab).show();

            })
            $('.firstTab li:first-child').addClass('active');
            $('.firstTab li').click(function() {
                $('.firstTab li').removeClass('active');
                $(this).addClass('active');
                $('.firstdisplay').hide();
                var currTab = $(this).attr('rel');
                $('#' + currTab).show();

            })
            $('.secondTab li:first-child').addClass('active');
            $('.secondTab li').click(function() {
                $('.secondTab li').removeClass('active');
                $(this).addClass('active');
                $('.seconddisplay').hide();
                var currTab = $(this).attr('rel');
                $('#' + currTab).show();

            })
            $('.thirdTab li:first-child').addClass('active');
            $('.thirdTab li').click(function() {
                $('.thirdTab li').removeClass('active');
                $(this).addClass('active');
                $('.thirddisplay').hide();
                var currTab = $(this).attr('rel');
                $('#' + currTab).show();

            })

            $('.fourTab li:first-child').addClass('active');
            $('.fourTab li').click(function() {
                $('.fourTab li').removeClass('active');
                $(this).addClass('active');
                $('.fourdisplay').hide();
                var currTab = $(this).attr('rel');
                $('#' + currTab).show();

            })




            $('.fiveTab li:first-child').addClass('active');
            $('.fiveTab li').click(function() {
                $('.fiveTab li').removeClass('active');
                $(this).addClass('active');
                $('.fivedisplay').hide();
                var currTab = $(this).attr('rel');
                $('#' + currTab).show();

            })


            $('.sixTab li:first-child').addClass('active');
            $('.sixTab li').click(function() {
                $('.sixTab li').removeClass('active');
                $(this).addClass('active');
                $('.sixdisplay').hide();
                var currTab = $(this).attr('rel');
                $('#' + currTab).show();

            })



            $('.sevenTab li:first-child').addClass('active');
            $('.sevenTab li').click(function() {
                $('.sevenTab li').removeClass('active');
                $(this).addClass('active');
                $('.sevendisplay').hide();
                var currTab = $(this).attr('rel');
                $('#' + currTab).show();

            })



            $('.eightTab li:first-child').addClass('active');
            $('.eightTab li').click(function() {
                $('.eightTab li').removeClass('active');
                $(this).addClass('active');
                $('.eightdisplay').hide();
                var currTab = $(this).attr('rel');
                $('#' + currTab).show();

            })



            $('.nineTab li:first-child').addClass('active');
            $('.nineTab li').click(function() {
                $('.nineTab li').removeClass('active');
                $(this).addClass('active');
                $('.ninedisplay').hide();
                var currTab = $(this).attr('rel');
                $('#' + currTab).show();

            })




            $('.tenTab li:first-child').addClass('active');
            $('.tenTab li').click(function() {
                $('.tenTab li').removeClass('active');
                $(this).addClass('active');
                $('.tendisplay').hide();
                var currTab = $(this).attr('rel');
                $('#' + currTab).show();

            })



            $('.elevenTab li:first-child').addClass('active');
            $('.elevenTab li').click(function() {
                $('.elevenTab li').removeClass('active');
                $(this).addClass('active');
                $('.elevendisplay').hide();
                var currTab = $(this).attr('rel');
                $('#' + currTab).show();

            })




            $('.mapView li:first-child').addClass('active');
            $('.mapView li').click(function() {
                    $('.mapView li').removeClass('active');
                    $(this).addClass('active');
                    $('.mapnone').hide();
                    var currTab4 = $(this).attr('rel');
                    $('#' + currTab4).show();

                })
                /*Tab JS End*/

            $(function() {
                var swiper = new Swiper('.headerBanner', {
                    pagination: '.swiper-pagination',
                    paginationClickable: true,
                    initialSlide: 0,
                    loop: false,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev'
                });
            });
            if (winW < 400) {
                swiper = new Swiper('.bankLogo', {
                    slidesPerView: 2,
                    initialSlide: 0,
                    loop: false,
                    spaceBetween: 10,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev'
                });
                $('#testDiv').slimScroll({
                    alwaysVisible: true,
                    railVisible: true,
                    height: '250px'
                });

            } else {
                var swiper = new Swiper('.bankLogo', {
                    slidesPerView: 4,
                    initialSlide: 0,
                    loop: false,
                    spaceBetween: 10,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev'
                });
            }

            if (winW < 800) {

                $('#testDiv').slimScroll({
                    alwaysVisible: true,
                    railVisible: true,
                    height: '250px'
                });

            }




            /*
            if(winW<400){
            	$(".iframe").colorbox({iframe:true, width:"30%", height:"20%"});
            }
            */




            //var swiper1 = new Swiper('.specSlider',{slidesPerView:1,initialSlide:0,loop:true,nextButton:'.swiper-button-next',prevButton:'.swiper-button-prev',});
            var swiper1 = new Swiper('.specSlider', {
                pagination: '.swiper-pagination',
                slidesPerView: 2,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev'
            });
            if (winW < 400) {
                var swiper1 = new Swiper('.specSlider', {
                    pagination: '.swiper-pagination',
                    slidesPerView: 1,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev'
                });
            }



            //var swiper = new Swiper('.gallerySwiper',{slidesPerView:4,initialSlide:0,loop:false,spaceBetween:3,nextButton:'.swiper-button-next',prevButton:'.swiper-button-prev'});

            var swiper = new Swiper('.gallerySwiper', {
                slidesPerView: 4,
                initialSlide: 0,
                loop: false,
                spaceBetween: 3,
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev'
            });
            if (winW < 400) {
                var swiper1 = new Swiper('.gallerySwiper', {
                    pagination: '.swiper-pagination',
                    slidesPerView: 1,
                    spaceBetween: 3,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev'
                });
            }



            $(document).ready(function() {
                if (winW > 400) {

                    var biggestHeight = 0;
                    $('.col-md-3').each(function() {
                        if ($(this).height() > biggestHeight) {
                            biggestHeight = $(this).height();
                        }
                    });
                    $('.col-md-3').height(biggestHeight);

                }
            });
                                    
        </script>

    <script>
      function initMap() {
          console.log("map load");
        var uluru = {lat: -25.363, lng: 131.044};
        var map = new google.maps.Map(document.getElementById('gmap'), {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAsjdyjEBilNNvLciAUa021HydzzFJToPU&callback=initMap">
    </script>
        
    </div>
    <script type="text/javascript">
        jQuery(document).ready(function (){           
           jQuery('#submit').click(function (){               
               var email_valid = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;   
               var name=jQuery('#name').val();
               var email=jQuery('#email').val();
               var message=jQuery('#message').val();
                if (name.length === 0) {                   
                   jQuery('#name').css({'border':"1px solid red"});
                   return false;
                }if (!email.match(email_valid) || email.length === 0) {
                   jQuery('#name').css({'border':"1px solid #ccc"});                    
                   jQuery('#email').css({'border':"1px solid red"});
                   return false;
                }if (message.length === 0) {
                   jQuery('#email').css({'border':"1px solid #ccc"});                    
                   jQuery('#message').css({'border':"1px solid red"});
                   return false;
                }else{   
                    jQuery('#name').css({'border':"1px solid #ccc"}); 
                    jQuery('#email').css({'border':"1px solid #ccc"}); 
                    jQuery('#message').css({'border':"1px solid #ccc"}); 
                    var content={};
                    content.name=name;
                    content.email=email;
                    content.message=message;
                    content.identity="ShreeNandContactUs";
                    
                    jQuery.ajax({
                        type: 'POST',
                        url: '<?php echo site_url();?>/ajaxcall',
                        contentType: "application/json",
                        dataType: 'JSON',
                        data: {name:name,email:email,message:message,identity:'ident'},//"name="+name+"&email="+email+"&message="+message+"&identity=cus",
                        success: function (data) {
                            
                        }
                    });                    
                }                  
           });                      
        });    

    </script>
<?php wp_footer(); ?>

</body>
</html>
