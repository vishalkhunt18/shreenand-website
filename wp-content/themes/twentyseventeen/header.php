<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
    <link href="<?php echo get_theme_file_uri("/css/style.css");?>" rel="stylesheet">
    <link href="<?php echo get_theme_file_uri("/css/custom.css");?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_theme_file_uri("/css/lightbox.css");?>">
    <link href="<?php echo get_theme_file_uri("/css/font-awesome.min.css");?>" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo get_theme_file_uri("/css/animate.min.css");?>">
    <link rel="stylesheet" href="<?php echo get_theme_file_uri("/css/colorbox.css");?>">
    <link rel="stylesheet" href="<?php echo get_theme_file_uri("/css/poiOnMapProject.css");?>">
   
    <script async="" src="<?php echo get_theme_file_uri("/js/beacon.js");?>"></script>
    <script type="text/javascript" src="<?php echo get_theme_file_uri("/js/jquery.min.js");?>"></script>
    <script type="text/javascript" src="<?php echo get_theme_file_uri("/js/jquery-1.11.0.min.js");?>"></script>
    <script type="text/javascript" src="<?php echo get_theme_file_uri("/js/swiper.js");?>"></script>
    <script type="text/javascript" src="<?php echo get_theme_file_uri("/js/jquery.js");?>"></script>
    <script type="text/javascript" src="<?php echo get_theme_file_uri("/js/jquery.colorbox.js");?>"></script>
<?php wp_head(); ?>
<script type="text/javascript">
        $(document).ready(function() {
            // Add smooth scrolling to all links
            $(".video-img a").on('click', function(event) {
                if (this.hash !== "") {
                    event.preventDefault();
                    var hash = this.hash;
                    $('html, body').animate({
                        scrollTop: $(hash).offset().top
                    }, 800, function() {
                        window.location.hash = hash;
                    });
                } // End if
            });
        });
    </script>
    <script type="text/javascript">
        winW = $(window).width();
        document.createElement('section');
        document.createElement('figure');
        document.createElement('aside');
        document.createElement('nav');
        document.createElement('header');
        document.createElement('footer');


        $(document).ready(function() {
            //Examples of how to assign the Colorbox event to elements
            //$(".group1").colorbox({rel:'group1'});
            $(".iframe").colorbox({
                iframe: true,
                width: "80%",
                height: "80%"
            });
            if (winW < 400) {
                $(".youtube").colorbox({
                    iframe: true,
                    innerWidth: 300,
                    innerHeight: 250
                });
            } else {
                $(".youtube").colorbox({
                    iframe: true,
                    innerWidth: 640,
                    innerHeight: 390
                });
            }

            if (winW < 400) {
                $(".group1").colorbox({
                    rel: 'group1',
                    width: "95%",
                    height: "250"
                });
                $(".group2").colorbox({
                    rel: 'group2',
                    width: "95%",
                    height: "250"
                });
                $(".group3").colorbox({
                    rel: 'group3',
                    width: "95%",
                    height: "250"
                });

            } else {
                $(".group1").colorbox({
                    rel: 'group1',
                });
                $(".group2").colorbox({
                    rel: 'group2',
                });
                $(".group3").colorbox({
                    rel: 'group3',
                });

            }


        })
    </script>        
</head>

<body <?php body_class(); ?>>
    <div class="main-Container">
        <div class="w100 shadow">
            <header class="header middleWrapper animated fadeInDown">
                <div class="topbrand">
                    <a class="logo" href="">
                        <!--<span>Shree</span> Nand-->
                        <img src="<?php echo get_theme_file_uri("images/logo.jpg");?>" width="193" height="60" alt="Shivalik Projects - Parkview">
                    </a>
                </div>
                <div class="nav-rhs"> <span class="bar">bar</span>
                    <ul class="navigation">
                        <li> <a href="#overview">Overview</a> </li>
                        <li class="displaynone"> <a class="iframe cboxElement" href="">Properties Available</a> </li>
                        <li> <a href="#amenities">Amenities</a> </li>
                        <li> <a href="#specifications">Specifications</a> </li>
                        <li> <a href="#unitdetail">Unit details</a> </li>
                        <li class="displaynone"> <a href="#mapview">Map View</a> </li>
                        <li> <a href="#imagegallery">Image Gallery</a> </li>
                        <li class="topcontact"> <a href="javascript:void(0);" class="contact sm-con">Contact Us</a> </li>
                    </ul>
                </div>
            </header>
        </div>
