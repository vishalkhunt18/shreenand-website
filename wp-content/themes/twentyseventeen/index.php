<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
        <?php // echo do_shortcode('[layerslider id="9"]');?>        
        <section class="rowSecond middleWrapper">
            <div class="cols1 animated bounceInLeft">
                <section class="greenBg">
                    <?php
                        global $post;
                        $args = array( 'numberposts' => 1, 'category_name' => 'overview' );
                        $posts = get_posts( $args );
//                        echo '<pre>';
//                        print_r($posts);
//                        echo '</pre>';
//                        exit;
                    ?>
                    <?php 
                        $field = get_field("left_side_content", $posts[0]->ID); 
                        echo $field;
                    ?>
                </section>
                <div class="video-img hidden">
                    <a class="youtube cboxElement" href="">
                        <?php
                        $field = get_field("left_side_image", $posts[0]->ID);                                                 
                        ?>
                        <img src="<?php echo $field;?>" alt="ShreeNand Heights" class="img-responsive" width="472" height="246">
                    </a>
                </div>
            </div>
            <div class="cols2" data-animation="animated zoomInUp">
                <div id="overview">&nbsp;</div>
                <h1><?php echo $posts[0]->post_title; ?></h1>
                <p>
                    <span style="font-size:16px; font-weight:bold; color:#103365;"> Offer: Pay 20% and Rest 80% on Possession </span>
                    <br> ShreeNand Heights - 3 BHK Apartments. We have given ourselves the liberty to create homes where families can rediscover the joy of togetherness and of spending precious times with each other without having the travel afar. Major Apartments garden facing for uniting you with nature. </p>
                <div class="download">
                    <ul>
                        <li> 
                            <?php $field = get_field("brochure", $posts[0]->ID); ?>
                            <a href="<?php echo $field;?>" target="_blank">Brochure</a>
                        </li>
                    </ul>
                </div>
                <div class="bank-approval">Bank Approvals</div>
                <div class="rel">
                    <div class="swiper-container bankLogo swiper-container-horizontal">
                        <div class="swiper-wrapper">
                            <?php 
                                $field = get_field("bank_approvals", $posts[0]->ID);
                                foreach($field as $item){                              
                            ?>
                            <div class="swiper-slide swiper-slide-active" style="width: 162.25px; margin-right: 10px;">
                                <img src="<?php echo $item["bank_images"];?>" alt="Bank Name" width="139" height="82">
                            </div>
                                <?php }?>
                        </div>
                    </div>
                    <div class="swiper-button-next bankless"></div>
                    <div class="swiper-button-prev bankless swiper-button-disabled"></div>
                </div>
            </div>
        </section>
        <div class="cb20"> </div>
        <div class="description middleWrapper">
            <?php
                $field = get_field("description_list", $posts[0]->ID);
                echo $field;
            ?>       
        </div>
        <section class="amenities-bg">
            <div class="middleWrapper">
                <div class="lhsAminities animated fadeInUp">
                    <div id="amenities">&nbsp;</div>
                    <?php 
                        global $post;
                        $args = array( 'numberposts' => 1, 'category_name' => 'amenities' );
                        $posts = get_posts( $args );                    
                    ?>
                    <h1><?php echo $posts[0]->post_title; ?></h1>
                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 380px;">
                        <div id="testDiv" style="overflow: hidden; width: auto; height: 380px;">
                            <ul>
                            <?php 
                                $field = get_field("amenities_list", $posts[0]->ID);
                                foreach($field as $item){
                            ?>    
                                <li><?php echo $item['amenities_list'];?></li>
                                <?php } ?>                                                                  
                            </ul>
                        </div>
                        <div class="slimScrollBar" style="background: rgb(92, 212, 104); width: 5px; position: absolute; top: 0px; opacity: 1; display: none; border-radius: 0px; z-index: 99; right: 1px; height: 380px;"></div>
                        <div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: block; border-radius: 0px; background: #103365; opacity: 1; z-index: 90; right: 1px;"></div>
                    </div>
                </div>
                <div class="rhsAminities animated fadeInDownBig">
                    <?php $field = get_field("right_side_image", $posts[0]->ID);?>
                    <img src="<?php echo $field;?>" alt="Parkview" width="477" height="436">
                </div>
            </div>
        </section>
        <section class="spec-bg">
            <div id="specifications">&nbsp;</div>
            <div class="middleWrapper">
                <?php 
                        global $post;
                        $args = array( 'numberposts' => 1, 'category_name' => 'specifications' );
                        $posts = get_posts( $args );                     
                ?>
                <h1>
                    <?php $posts[0]->post_title;?>
                </h1>
                <div class="specSlider swiper-container swiper-container-horizontal">
                    <div class="swiper-wrapper">
                        <?php
                            $field = get_field("specifications", $posts[0]->ID);
                            $i=0;
                            foreach($field as $item){
                                if($i=0){
                                    $class="swiper-slide-active";
                                }else if($i==1){
                                    $class="animated fadeInDownBig swiper-slide-next";
                                }else{
                                     $class="animated fadeInDownBig";
                                }                 
                        ?>
                        <div class="swiper-slide <?php echo $class;?>" style="width: 585px;">
                            <div class="lhsSpec animated fadeInDownBig">                                
                              <?php echo $item["specification"];?>
                            </div>
                        </div>
                            <?php $i++;}?>
                    </div>
                </div>
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev swiper-button-disabled"></div>
        </section>
        <div class="middleWrapper">
            <div class="unit">
                <div id="unitdetail">&nbsp;</div>
                <?php
                        global $post;
                        $args = array( 'numberposts' => 15, 'category_name' => 'unit-details' );
                        $posts = get_posts( $args );                             
                ?>
                <h1>
                    <?php
                        $catObj = get_category_by_slug('unit-details');  
                        echo $catObj->name;
                    ?>
                </h1>
            </div>
            <section class="allTabs">
                <div class="mainTab">
                    <ul>
                        <?php
                            $i=1;
                            foreach($posts as $item){
                        ?>
                        <li rel="tab<?php echo $i;?>" class="<?php if($i==0){echo 'active';}?>">
                            <?php echo $item->post_title;?>
                        </li>
                        <?php $i++; } ?>
                    </ul>
                </div>
                <div class="allNone">
                    <?php 
                        $i=1;
                        foreach($posts as $item){                            
                    ?>
                    <div class="nodisplay sodisplay <?php if($i != 1){echo "hide";} ?>" id="tab<?php echo $i;?>">
                        <ul class="firstTab">
                            <li rel="firstTab1" >
                                <?php 
                                    $field = get_field("unit_area", $item->ID);
                                    echo $field;
                                ?>
                            </li>
                        </ul>
                        <div class="cb"></div>
                        <div class="firstdisplay sodisplay" id="firstTab1">
                            <div class="floorplan-con">
                                <div class="lhsFloor">
                                    <?php 
                                        $field = get_field("unit_plan/image", $item->ID);
                                    ?>
                                    <a class="group3 cboxElement" href="<?php echo $field;?>">
                                        <img src="<?php echo $field;?>" alt="Floor Plan" width="400" height="400"/>
                                            <span>
                                                <img src="<?php echo get_theme_file_uri("/images/zoom.png");?>" alt="" width="20" height="20"/>
                                            </span>
                                    </a>
                                </div>
                                <div class="rhsFloor">
                                    <?php echo $item->post_content; ?>                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++;}?>                               
                </div>
            </section>
        </div>
        <div class="Map middleWrapper">
                <?php
                        global $post;
                        $args = array( 'numberposts' => 15, 'category_name' => 'map-view' );
                        $posts = get_posts( $args );                             
                ?>
                <h1 id="mapview">
                    <?php
                        $catObj = get_category_by_slug('map-view');  
                        echo $catObj->name;
                    ?>
                </h1>            
            
            <div class="mapView">
                <ul>
                    <?php
                        $i=1;
                        foreach($posts as $item){
                    ?>
                    <li rel="mapTab<?php echo $i;?>" class="<?php if($i==1){echo "active";}?>"><?php echo $item->post_title;?></li>
                    <?php $i++;}?>
                </ul>
            </div>
            <div class="map-con">
                <?php
                    $i=1;
                    foreach ($posts as $item){                        
                ?>
                <div class="mapnone <?php if($i !=1){echo "hide";}?>" id="mapTab<?php echo $i;?>" <?php if($i==1){echo "style='display:block'";}?>> 
                    <?php
                        $field = get_field("images", $item->ID);
                        if($field != ""){
                    ?>
                    <img src="<?php echo $field;?>" width="788" height="534">
                    <?php }else{?>
                    <?php echo $item->post_content; ?>
                    <?php }?>
                </div>
                <?php $i++;}?>              
            </div>
        </div>
        <div class="gallery middleWrapper animated fadeInDownBig" id="imagegallery">
                <?php
                        global $post;
                        $args = array( 'numberposts' => 15, 'category_name' => 'Gallery' );
                        $posts = get_posts( $args );                             
                ?>            
            <h1><?php echo $posts[0]->post_title;?></h1>
            <div class="swiper-container1">
                <div class="swiper-container gallerySwiper swiper-container-horizontal">
                    <div class="swiper-wrapper">
                        <?php 
                            $i=0;
                            $field = get_field("images", $posts[0]->ID);
                            foreach($field as $item){
                                if($i=0){
                                    $class="swiper-slide-active";
                                }else if($i=1){
                                    $class="swiper-slide-next";
                                }else{
                                    $class="";
                                }                                
                        ?>
                        
                        <div class="swiper-slide <?php echo $class;?> slide-gal">
                            <a class="group2 cboxElement" href="<?php echo $item["gallery_images"];?>">
                                <img src="<?php echo $item["gallery_images"];?>" alt="" width="300" height="278">
                                <div class="hovereffect"><i class="fa fa-search"></i>
                                </div>
                            </a>
                        </div>
                        <?php }?>
                
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev swiper-button-disabled"></div>
                </div>
            </div>
        </div>
        <div class="overlay"> </div>
        <div class="cal-bg">
            <div class="cal middleWrapper">
                <h1>EMI Calculator</h1>
                <h2>How much do i need to pay as EMI?</h2>
                <h3>Here we are to help you calculate the amount
you need to pay each month for a amount taken as loan
</h3>
                <div class="emifrmae fadeIn wow animated">
                    <iframe id="large-frame" src="<?php echo get_theme_file_uri("/frame/calculator.html");?>" style="visibility:visible; width:100%; height:300px; overflow:hidden;" scrolling="no" frameborder="0"></iframe>
                </div>
            </div>
        </div>
        <div class="about-developer middleWrapper">
                <?php
                        global $post;
                        $args = array( 'numberposts' => 1, 'category_name' => 'about' );
                        $posts = get_posts( $args );                             
                ?>              
            <h1><?php echo $posts[0]->post_title;?></h1>
            <div class="dev-con">
                <?php 
                    echo $posts[0]->post_content;
                ?>

            </div>
        </div>
        <div class="bottom-icon">

<?php get_footer();
