wow = new WOW({
    animateClass: 'animated',
    offset: 100,
    callback: function(box) {
        console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
    }
});
wow.init();
$(document).ready(function() {
    $("a[href='#']").on("click", function(e) {
        e.preventDefault();
    });
    var menuValue = 0;
    $(".navbar-toggle").click(function() {
        if (menuValue == 0) {
            menuValue = 1;
            console.log("menuValue -> ", menuValue);
        } else {
            menuValue = 0;
            console.log("menuValue -> ", menuValue);
        }
        $(this).toggleClass("navactive");
        $(this).next("nav").slideToggle(500);
    });
    $("header nav a").click(function() {
        $("header nav a").removeClass("active");
        $(this).addClass("active");
        if (menuValue == 1) {
            menuValue = 0;
            console.log("menuValue -> ", menuValue);
            $("header nav").slideUp(500);
        }
    });
    $("#contact-form-top input:text, #contact-form-bottom input:text, #contact-form-mid input:text, #contact-lb input:text").focus(function() {
        $(this).parents(".col-md-3").addClass("input-focused");
    });
    $("#contact-form-top input:text, #contact-form-bottom input:text, #contact-form-mid input:text, #contact-lb input:text").blur(function() {
        var val = $(this).val();
        if (val == "") {
            $(this).parents(".col-md-3").removeClass("input-focused");
        } else {
            $(this).parent(".col-md-3").addClass("hasContent");
        }
    });
    $('a.anchorT').click(function() {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top - 100
        }, 1000);
        return false;
    });
    $("#homeLoanAmount").slider({
        min: 500000,
        max: 10000000,
        range: "min",
        value: 2500000,
        create: function(event, ui) {
            $('#homeLoanAmountInput').val(2500000);
        },
        slide: function(event, ui) {
            $('#homeLoanAmountInput').val(ui.value);
            $('#amount-words').html(ui.value);
        }
    });
    $("#homeLoanAmount").slider({
        min: 500000,
        max: 10000000,
        range: "min",
        value: 2500000,
        stop: function(event, ui) {
            showpay();
        }
    });
    $("#homeLoanAmountInput").change(function() {
        var thisVal = $(this).val();
        if (thisVal < 500000 || thisVal > 10000000) {
            $(this).val(2500000);
            $("#homeLoanAmount").slider("value", $(this).val());
            alert('Price Not Valid');
        } else {
            $("#homeLoanAmount").slider("value", $(this).val());
        }
    });
    $('#interestRateInput').val(10);
    $("#interestRate").slider({
        min: 5,
        max: 30,
        range: "min",
        value: 10,
        create: function(event, ui) {
            $('#interestRateInput').val(10);
        },
        slide: function(event, ui) {
            $('#interestRateInput').val(ui.value);
        }
    });
    $("#interestRate").slider({
        min: 5,
        max: 30,
        range: "min",
        value: 10,
        stop: function(event, ui) {
            showpay();
        }
    });
    $("#interestRateInput").change(function() {
        var thisVal = $(this).val();
        if (thisVal < 5 || thisVal > 30) {
            $(this).val(10);
            $("#interestRate").slider("value", $(this).val());
            alert('Price Not Valid');
        } else {
            $("#interestRate").slider("value", $(this).val());
        }
    });
    $('#loanTenureInput').val(15);
    $("#loanTenure").slider({
        min: 5,
        max: 25,
        range: "min",
        value: 15,
        create: function(event, ui) {
            $('#loanTenureInput').val(15);
        },
        slide: function(event, ui) {
            $('#loanTenureInput').val(ui.value);
        }
    });
    $("#loanTenure").slider({
        min: 5,
        max: 25,
        range: "min",
        value: 15,
        stop: function(event, ui) {
            showpay();
        }
    });
    $("#loanTenureInput").change(function() {
        var thisVal = $(this).val();
        if (thisVal < 5 || thisVal > 25) {
            $(this).val(15);
            $("#loanTenure").slider("value", $(this).val());
            alert('Price Not Valid');
        } else {
            $("#loanTenure").slider("value", $(this).val());
        }
    });
    var iWords = ['Zero', ' One', ' Two', ' Three', ' Four', ' Five', ' Six', ' Seven', ' Eight', ' Nine'];
    var ePlace = ['Ten', ' Eleven', ' Twelve', ' Thirteen', ' Fourteen', ' Fifteen', ' Sixteen', ' Seventeen', ' Eighteen', ' Nineteen'];
    var tensPlace = ['', ' Ten', ' Twenty', ' Thirty', ' Forty', ' Fifty', ' Sixty', ' Seventy', ' Eighty', ' Ninety'];
    var inWords = [];
    var numReversed, inWords, actnumber, i, j;

    function tensComplication() {
        'use strict';
        if (actnumber[i] === 0) {
            inWords[j] = '';
        } else if (actnumber[i] === 1) {
            inWords[j] = ePlace[actnumber[i - 1]];
        } else {
            inWords[j] = tensPlace[actnumber[i]];
        }
    }

    function numberWords(idVal, idcontainer) {
        'use strict';
        var junkVal = idVal;
        junkVal = Math.floor(junkVal);
        var obStr = junkVal.toString();
        numReversed = obStr.split('');
        actnumber = numReversed.reverse();
        if (Number(junkVal) >= 0) {} else {
            return false;
        }
        if (Number(junkVal) === 0) {
            document.getElementById('container').innerHTML = obStr + '' + 'Rupees Zero';
            return false;
        }
        if (actnumber.length > 9) {
            return false;
        }
        var iWordsLength = numReversed.length;
        var finalWord = '';
        j = 0;
        for (i = 0; i < iWordsLength; i++) {
            switch (i) {
                case 0:
                    if (actnumber[i] === '0' || actnumber[i + 1] === '1') {
                        inWords[j] = '';
                    } else {
                        inWords[j] = iWords[actnumber[i]];
                    }
                    inWords[j] = inWords[j];
                    break;
                case 1:
                    tensComplication();
                    break;
                case 2:
                    if (actnumber[i] === '0') {
                        inWords[j] = '';
                    } else if (actnumber[i - 1] !== '0' && actnumber[i - 2] !== '0') {
                        inWords[j] = iWords[actnumber[i]] + ' Hundred and';
                    } else {
                        inWords[j] = iWords[actnumber[i]] + ' Hundred';
                    }
                    break;
                case 3:
                    if (actnumber[i] === '0' || actnumber[i + 1] === '1') {
                        inWords[j] = '';
                    } else {
                        inWords[j] = iWords[actnumber[i]];
                    }
                    if (actnumber[i + 1] !== '0' || actnumber[i] > '0') {
                        inWords[j] = inWords[j] + ' Thousand';
                    }
                    break;
                case 4:
                    tensComplication();
                    break;
                case 5:
                    if (actnumber[i] === '0' || actnumber[i + 1] === '1') {
                        inWords[j] = '';
                    } else {
                        inWords[j] = iWords[actnumber[i]];
                    }
                    if (actnumber[i + 1] !== '0' || actnumber[i] > '0') {
                        inWords[j] = inWords[j] + ' Lakh';
                    }
                    break;
                case 6:
                    tensComplication();
                    break;
                case 7:
                    if (actnumber[i] === '0' || actnumber[i + 1] === '1') {
                        inWords[j] = '';
                    } else {
                        inWords[j] = iWords[actnumber[i]];
                    }
                    inWords[j] = inWords[j] + ' Crore';
                    break;
                case 8:
                    tensComplication();
                    break;
                default:
                    break;
            }
            j++;
        }
        inWords.reverse();
        for (i = 0; i < inWords.length; i++) {
            finalWord += inWords[i];
        }
        document.getElementById(idcontainer).innerHTML = finalWord;
    }
    setInterval(function() {
        var homeLoanAmount = $("#homeLoanAmountInput").val();
        numberWords(homeLoanAmount, "amount-words");
    }, 100);
    $(".tabs li a").click(function() {        
        var ind = $(this).parent("li").index();
        $(this).parents(".tabs").next(".tabs-data").children("li").removeClass("active");
        $(this).parents(".tabs").next(".tabs-data").children("li").eq(ind).addClass("active");
        $(this).parents(".tabs").children("li").children("a").removeClass("active");
        $(this).addClass("active");
    });
    var udGalleryPopSize = 500;
    if ($(window).width() < 500) {
        udGalleryPopSize = "auto";
    } else {
        udGalleryPopSize = 500
    }
//    $('.fancybox-buttons').fancybox({
//        openEffect: 'none',
//        closeEffect: 'none',
//        prevEffect: 'none',
//        nextEffect: 'none',
//        closeBtn: true,
//        minWidth: udGalleryPopSize,
//        minHeight: udGalleryPopSize,
//        helpers: {
//            title: {
//                type: 'inside',
//                position: 'top'
//            },
//            buttons: {}
//        },
//        afterLoad: function() {
//            this.title = 'Project Photos - Image ' + (this.index + 1) + '/' + this.group.length + (this.title ? ' - ' + this.title : '');
//            $(".fancybox-inner").addClass("fancybox-outer-ud");
//        }
//    });
	
//	$('.properties').fancybox({
//        openEffect: 'none',
//        closeEffect: 'none',
//        prevEffect: 'none',
//        nextEffect: 'none',
//        closeBtn: true,
//        helpers: {
//            title: {
//                type: 'inside',
//                position: 'top'
//            },
//            buttons: {}
//        },
//        afterLoad: function() {
//            this.title = 'Properties Available'
//            $(".fancybox-inner").addClass("contact-lightbox");
//        }
//    });
//	
//	
//    $('.contactFormfb').fancybox({
//        openEffect: 'none',
//        closeEffect: 'none',
//        prevEffect: 'none',
//        nextEffect: 'none',
//        closeBtn: true,
//        helpers: {
//            title: {
//                type: 'inside',
//                position: 'top'
//            },
//            buttons: {}
//        },
//        afterLoad: function() {
//            this.title = 'Contact'
//            $(".fancybox-inner").addClass("contact-lightbox");
//        }
//    });
//    $('.fancybox-gallery').fancybox({
//        openEffect: 'none',
//        closeEffect: 'none',
//        prevEffect: 'none',
//        nextEffect: 'none',
//        closeBtn: true,
//        helpers: {
//            title: {
//                type: 'inside',
//                position: 'top'
//            },
//            buttons: {}
//        },
//        beforeShow: function() {
//            $(".fancybox-overlay").addClass("fb-gallery-popup");
//            $(".fancybox-skin").css({
//                "padding": 0
//            });
//        }
//    });
    $(document.body).on("click", ".gallery-main-img", function() {
        $(".gallery-main-img").fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            prevEffect: 'none',
            nextEffect: 'none',
            closeBtn: true,
            helpers: {
                title: {
                    type: 'inside',
                    position: 'top'
                },
                buttons: {}
            },
            beforeShow: function() {
                $(".fancybox-wrap").addClass("fb-gallery-popup-fullview");
            }
        });
    });
    var status = false;

    function textValidation(id, error, event) {
        var textVal = $(id).val();
        textVal = $.trim(textVal);
        var letters = /^[A-Za-z ]+$/;
        if (textVal == "") {
            $(id).next(".form-error-msg-box").html(error);
            event.preventDefault();
            return false;
        } else {
            if (textVal.match(letters)) {
                return true;
            } else {
                $(id).next(".form-error-msg-box").html("Please enter only alphabets.");
                event.preventDefault();
                return false;
            }
        }
        return true;
    }

    function numberValidation(id, error, event) {
        var numbers = /[1-9][0-9-]{9,14}/;
        var mobVal = $(id).val();
        if (mobVal.match(numbers)) {
            $(id).next(".form-error-msg-box").html("");
            return true;
        } else {
            $(id).next(".form-error-msg-box").html(error);
            event.preventDefault();
            return false;
        }
    }

    function emailValidation(id, error, event) {
        var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var emailVal = $(id).val();
        if (!filter.test(emailVal)) {
            $(id).next(".form-error-msg-box").html('Please provide a valid email address.');
            event.preventDefault();
            return false;
        } else {
            $(id).next(".form-error-msg-box").html("");
            return true;
        }
    }
    $("#contact-form-top #submit-contact").click(function(event) {
        if (textValidation("#name", "Please enter Name.", event)) {}
        if (numberValidation("#mobile", "Please enter Mobile.", event)) {}
        if (emailValidation("#email", "Please enter E-mail.", event)) {}
    });
    $("#contact-form-bottom #submit-contact-bottom").click(function(event) {
        if (textValidation("#Name1", "Please enter Name.", event)) {}
        if (numberValidation("#Mobile1", "Please enter Mobile.", event)) {}
        if (emailValidation("#Email1", "Please enter E-mail.", event)) {}
    });
    $("#contact-form-mid #submit-contact-mid").click(function(event) {
        if (textValidation("#Name2", "Please enter Name.", event)) {}
        if (numberValidation("#Mobile2", "Please enter Mobile.", event)) {}
        if (emailValidation("#Email2", "Please enter E-mail.", event)) {}
    });
    $("#contact-lb #submit-contact-lb").click(function(event) {
        if (textValidation("#name4", "Please enter Name.", event)) {}
        if (numberValidation("#mobile4", "Please enter Mobile.", event)) {}
        if (emailValidation("#email4", "Please enter E-mail.", event)) {}
    });
    $(".title-clck").click(function() {
        $(this).next(".fm1").slideToggle(500);
		
    });
	
	$(".title-clck1").click(function() {
        $(this).next(".formasd").slideToggle(500);
		
    });
	
	
    $('#slider').nivoSlider({
        effect: 'random',
        slices: 15,
        boxCols: 10,
        boxRows: 5,
        animSpeed: 1000,
        pauseTime: 70000,
        startSlide: 0,
        directionNav: true,
        controlNav: true,
        controlNavThumbs: false,
        pauseOnHover: false,
        manualAdvance: false,
        prevText: 'Prev',
        nextText: 'Next',
        randomStart: false
    });
    
//    $("#contact-form-mid .form-mid-button").click(function() {
//        $(this).next(".row-form").toggle();
//    });
//    var headerHgt = $("#slider-home").height();
//    headerHgt = headerHgt - 150;
//    var windowHgt = $(document).height();
//    var footerHgt = $("#contact-form-bottom").height();
//    footerHgt += 100;
//    var aboutDeveloper = $(".about-content").offset();
//    var aboutDeveloperPosition = aboutDeveloper.top;
//    var winHeight = $(window).height();
//    aboutDeveloperPosition = aboutDeveloperPosition - winHeight;
//    aboutDeveloperPosition += 100;
//    $(window).scroll(function() {
//        var scrl = $(window).scrollTop();
//        scrl += 50;
//        var scrlMove = $(window).scrollTop() + winHeight;
//        if ($(window).scrollTop() > headerHgt && $(window).scrollTop() < aboutDeveloperPosition) {
//            $("#contact-form-mid").addClass("contact-show");
//        } else {
//            $("#contact-form-mid").removeClass("contact-show");
//        }
//    });
//    $("html,body").on("click", function(event) {
//        if (!$(event.target).parents().hasClass("contact-form-mid")) {
//            $("#contact-form-mid .row-form").fadeOut(500);
//        }
//    });
//    $('.specify-jcarousel').on('jcarousel:create jcarousel:reload', function() {
//        var element = $(this),
//            width = element.innerWidth();
//        if (width > 900) {
//            width = width / 2;
//        } else if (width > 767) {
//            width = width / 2;
//        }
//        element.jcarousel('items').css('width', width + 'px');
//    }).jcarousel({});
//    $('.specify-jcarousel-prev').jcarouselControl({
//        target: '-=1'
//    });
//    $('.specify-jcarousel-next').jcarouselControl({
//        target: '+=1'
//    });
//	
//    $('.unit-details-jcarousel').on('jcarousel:create jcarousel:reload', function() {
//        var element = $(this),
//            widthUd = element.innerWidth();
//        if (widthUd > 990) {
//            widthUd = widthUd / 3;
//        } else if (widthUd > 687) {
//            widthUd = widthUd / 2;
//        } else if (widthUd < 687) {
//            widthUd = widthUd / 1;
//        }
//        element.jcarousel('items').css('width', widthUd + 'px');
//    }).jcarousel({});
//    $('.ud-jcarousel-control-prev').jcarouselControl({
//        target: '-=1'
//    });
//    $('.ud-jcarousel-control-next').jcarouselControl({
//        target: '+=1'
//    });
//	
//	
//	$('.spJcarousel').on('jcarousel:create jcarousel:reload', function() {
//        var element = $(this),
//            widthUd = element.innerWidth();
//        if (widthUd > 990) {
//            widthUd = widthUd / 5;
//        } else if (widthUd > 687) {
//            widthUd = widthUd / 2;
//        } else if (widthUd < 687) {
//            widthUd = widthUd / 1;
//        }
//        element.jcarousel('items').css('width', widthUd + 'px');
//    }).jcarousel({});
//    $('.sp-jcarousel-control-prev').jcarouselControl({
//        target: '-=1'
//    });
//    $('.sp-jcarousel-control-next').jcarouselControl({
//        target: '+=1'
//    });
//	
//	
//	
	
	
    setTimeout(function() {
        $("#slider-home").removeClass("opacityNone");
        $(".loader-slider").remove();
    }, 1500);
    setTimeout(function() {
        $("#contact-form-top").removeClass("contactOpacityNone");
    }, 2000);
    $("header nav a").each(function() {
        var navAnchorWidth = $(this).innerWidth();
        $(this).css("min-width", navAnchorWidth);
        $(this).addClass("anchorPaddingNone");
    });

    function validateNumber(event) {
        console.log("dkdk -> ", event.which);
        if (event.shiftKey == 0) {
            if ((event.which != 48 && event.which != 96) && (event.which != 49 && event.which != 97) && (event.which != 50 && event.which != 98) && (event.which != 51 && event.which != 99) && (event.which != 52 && event.which != 100) && (event.which != 53 && event.which != 101) && (event.which != 54 && event.which != 102) && (event.which != 55 && event.which != 103) && (event.which != 56 && event.which != 104) && (event.which != 57 && event.which != 105) && (event.which != 8)) {
                event.preventDefault();
            }
        } else {
            event.preventDefault();
        }
    }
    $('#mobile, #Mobile2, #mobile4, #Mobile1').keydown(function(event) {
        validateNumber(event);
    });
   

    /*function pageRedirection() {
        var winWidth = $(window).width();
        var pageUrl = window.location.href;
        pageUrl = pageUrl.split("/");
        pageUrl = pageUrl.slice(-1);
        if ((navigator.userAgent.match(/iPhone/i)) && winWidth < 768 && pageUrl == "index.html" || (navigator.userAgent.match(/Android/i)) && winWidth < 768 && pageUrl == "index.html") {
            document.location = "index-mobile.html";
        }
        if ((navigator.userAgent.match(/iPhone/i)) && winWidth < 768 && pageUrl == "" || (navigator.userAgent.match(/Android/i)) && winWidth < 768 && pageUrl == "") {
            document.location = "index-mobile.html";
        }
        if (((navigator.userAgent.match(/Android/i)) && winWidth >= 768 && pageUrl == "index-mobile.html") || ((navigator.userAgent.match(/Windows/i)) && pageUrl == "index-mobile.html")) {
            document.location = "index.html";
        }
    }
    pageRedirection();*/
});
$(window).load(function() {
    var headerHeight = $("header").innerHeight();
    var sliderHeight = $("#slider-home").innerHeight();
    var hHeight = headerHeight + sliderHeight;
    $(window).scroll(function() {
        if ($(window).scrollTop() > headerHeight) {
            $("header").addClass("f-nav");
        } else {
            $("header").removeClass("f-nav");
        }
    });
    setInterval(function() {
        $(".arrow-down-icon").toggleClass("active");
    }, 500);
    $(".arrow-down-icon").click(function() {
        $('html, body').animate({
            scrollTop: sliderHeight - 51
        }, 800);
    });
    $(document.body).on("click", "#gallery-main-jcarousel li a", function() {
        var a = $(this).parent("li").index();
        $(".rg-thumbs").find("li").eq(a).children("a").click();
    });
    var listLength = $(".es-carousel li").length;
    var tText = "Project Photos- Image 1/" + listLength;
    $(".title-gallery").html(tText);
    $(".es-carousel li a").on("click", function() {
        var listIndex = $(this).parent("li").index();
        listIndex += 1;
        var tText2 = "Project Photos- Image " + listIndex + "/" + listLength;
        $(".title-gallery").html(tText2);
    });

    function amenitiesTitle() {
        var jcarouselIW = $(".jcarousel").innerWidth();
        var amenitiesCounter = 0;
        var amenitiesList = $(".jcarousel ul li").children(".col-md-12").length;
        if (jcarouselIW >= 800) {
            amenitiesCounter = 4;
        } else if (jcarouselIW >= 600) {
            amenitiesCounter = 3;
        } else if (jcarouselIW >= 350) {
            amenitiesCounter = 2;
        } else if (jcarouselIW <= 322) {
            amenitiesCounter = 1;
        }
        amenitiesCounter = amenitiesCounter * 2;
        amentiesTitle = "Amentities " + amenitiesCounter + "/" + amenitiesList;
        $("#amentities-carousel").children(".title").children("span").html(amentiesTitle);
    }
    $("#amentities").on("click", ".arrow-icon", function() {
        $("#amentities-carousel .col-md-12").removeClass("wow");
        $("#amentities-carousel .col-md-12").removeClass("fadeInUp");
        $("#amentities-carousel .col-md-12").removeClass("fadeInDown");
        $("#amentities-carousel .col-md-12").removeAttr("style");
    });
    amenitiesTitle();
    $(window).resize(function() {
        amenitiesTitle();
    });
});

function showpay() {
    document.getElementById("emiMonthly").innerHTML = '';
    document.getElementById("totIntPay").innerHTML = '';
    document.getElementById("totPay").innerHTML = '';
    var princ = 2500000;
    var term = 15;
    var intr = 10 / 1200;
    if (document.getElementById("homeLoanAmountInput").value != '') {
        princ = document.getElementById("homeLoanAmountInput").value;
    }
    if (document.getElementById("loanTenureInput").value != '') {
        term = document.getElementById("loanTenureInput").value;
    }
    if (document.getElementById("interestRateInput").value != '') {
        intr = document.getElementById("interestRateInput").value / 1200;
    }
    var emi = princ * intr / (1 - (Math.pow(1 / (1 + intr), term * 12)));
    var rs = '<span class="icon-ruppee"></span> &nbsp;';
    var emiDisp = rs + Math.round(emi);
    var totalInrPay = Math.round(emi * (term * 12));
    var totalInrPayDisp = rs + (totalInrPay - princ);
    var totalPay = totalInrPay;
    var totalPayDis = rs + totalPay;
    var monthly_emiHidValue = emiDisp;
    var mEmiValue = monthly_emiHidValue.split("&nbsp;");
    mEmiValue = mEmiValue[1];
    document.getElementById("monthly_emi").value = mEmiValue;
    var tpi_HidValue = totalInrPayDisp;
    var tipValue = tpi_HidValue.split("&nbsp;");
    tipValue = tipValue[1];
    document.getElementById("totalInterestPayable").value = tipValue;
    var totalPayment_HidValue = totalPayDis;
    var tpValue = totalPayment_HidValue.split("&nbsp;");
    tpValue = tpValue[1];
    document.getElementById("totalPayment").value = tpValue;
    document.getElementById("emiMonthly").innerHTML += emiDisp + " ";
    document.getElementById("totIntPay").innerHTML += totalInrPayDisp + " ";
    document.getElementById("totPay").innerHTML += totalPayDis + " ";
}
showpay();
var a = ['', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine ', 'ten ', 'eleven ', 'twelve ', 'thirteen ', 'fourteen ', 'fifteen ', 'sixteen ', 'seventeen ', 'eighteen ', 'nineteen '];
var b = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

function inWords(num, container) {
    if ((num = num.toString()).length > 9) return 'overflow';
    n = ('000000000' + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return;
    var str = '';
    str += (n[1] != 0) ? (a[Number(n[1])] || b[n[1][0]] + ' ' + a[n[1][1]]) + 'crore ' : '';
    str += (n[2] != 0) ? (a[Number(n[2])] || b[n[2][0]] + ' ' + a[n[2][1]]) + 'lakh ' : '';
    str += (n[3] != 0) ? (a[Number(n[3])] || b[n[3][0]] + ' ' + a[n[3][1]]) + 'thousand ' : '';
    str += (n[4] != 0) ? (a[Number(n[4])] || b[n[4][0]] + ' ' + a[n[4][1]]) + 'hundred ' : '';
    str += (n[5] != 0) ? ((str != '') ? 'and ' : '') + (a[Number(n[5])] || b[n[5][0]] + ' ' + a[n[5][1]]) + '' : '';
    $(container).text(str)
}
setInterval(function() {
    var monthly_emiValue = $('#monthly_emi').val();
    inWords(monthly_emiValue, "#monthly_emiText");
    var totalInterestPayableValue = $('#totalInterestPayable').val();
    inWords(totalInterestPayableValue, "#totalInterestPayableText");
    var totalPaymentValue = $('#totalPayment').val();
    inWords(totalPaymentValue, "#totalPaymentText");
}, 500);







$(function(){
    jQuery('img.svg').each(function(){
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
    
        jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');
    
            // Add replaced image's ID to the new SVG
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            // Add replaced image's classes to the new SVG
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
    
            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');
            
            // Check if the viewport is set, else we gonna set it if we can.
            if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
                $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
            }
    
            // Replace image with new SVG
            $img.replaceWith($svg);
    
        }, 'xml');
    
    });
});


$(document).ready(function(){
	
	$(document).on('click','.readBtn',function(){
		var btnText = $(this);
		$('.textBox').slideToggle(function(){
			if($(this).is(':visible'))
			{
				$(btnText).text('Read Less');
			}
			else{
				$(btnText).text('Read More');
			}
		});	
	});
	
	
	$(document).on('click','.readBtn1',function(){
		var btnText = $(this);
		$('.textBox1').slideToggle(function(){
			if($(this).is(':visible'))
			{
				$(btnText).text('Read Less');
			}
			else{
				$(btnText).text('Read More');
			}
		});	
	});
	
	
	
});




$(window).load(function(){
//map tab start
$('#tabMap li a').bind('click',function(e){
	$('#tabMap li a').removeClass('active');
	$(this).addClass('active');
	var tabid=$(this).attr('data-map');
	$('.tabCon').hide();
	$('.tabContainer').find(tabid).show();
	$('html, body').animate({ scrollTop:$(this).offset().top-80}, 1000);
});
//map tab end
	
});
