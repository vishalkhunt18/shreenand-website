<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>


<div class="about-developer middleWrapper main-page-content">
<?php 

$post = get_post(get_the_ID()); 
?>                              
    <h1 class="main-page-content-h1"><?php echo $post->post_title;?></h1>
        <div class="dev-con">
               <?php echo $post->post_content;?>
        </div>
</div>
<?php get_footer();
