<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'shreenand');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'J|Uw~ Jxj,~6A1mm,1h?|VClgM3b@rz*L[:cQPC]4*~#eT*g0(1W!riX&-dRho+l');
define('SECURE_AUTH_KEY',  'C;C;+B*Hi3~Jy61ux+ckQmuIvH5jg1^O9bR,GQ[}M4KWS.jPp_T.(&qy:|B< l9^');
define('LOGGED_IN_KEY',    'bX^(G@z:CPRuDfHahg@[^MfjTh/fz<d*M@@w5]W^r0nY J|*b*E`e/&$ZokcSdMY');
define('NONCE_KEY',        ';?/2$u8a7A^=Z=L6]^^{3xZgt]B*74X92Xq)[V-Du{Emh^?dA}>N9i>:!1sX4{tA');
define('AUTH_SALT',        '*wq4:q @Sj,fh -{#dtWCtI50;5k)`rC1,7& >o@]1:0Ji^{Fw]NdmbH3@JS&!N:');
define('SECURE_AUTH_SALT', '``7b{0hey#vv.6$w8BaN`8@ls+.5qR7cv?(/7x^bvgZ:p6^5D]%$-]jToJpdusCF');
define('LOGGED_IN_SALT',   '0.c?nlg0@q>ctd%<8vB#3fq&>`+Tp^zEN~l1/RT3w@*EzJG26r5{-f]1(C-:mRHL');
define('NONCE_SALT',       '+Aa0  v p%e3@T&(0d:I/+JK3iXC{uC#cjTS&QWwaBOISJMIDcpLu%`PSIVf|yb9');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'si_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
